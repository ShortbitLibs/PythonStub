# Imports
import itertools
import json
import sys
from enum import Enum
from io import StringIO

import os
from typing import Tuple, Set, Optional, Dict

from google.protobuf.descriptor_pb2 import DescriptorProto, EnumDescriptorProto, FileDescriptorProto, \
    FieldDescriptorProto

import plugin_pb2 as plugin

__author__ = 'Fabian Schreier'
__version__ = "0.1.0"
__status__ = "Prototype"

class IndentContextManager:
    def __init__(self, writer: 'IndentedWriter'):
        self._writer = writer

    def __enter__(self):
        self._writer.indent()

    def __exit__(self, exc_type, exc_val, exc_tb):
        self._writer.dedent()


class IndentedWriter:
    def __init__(self, buffer: StringIO = None, tab: str = '    '):
        self._buffer = buffer or StringIO()
        self._indent = 0
        self._tab = tab
        self._indent_context = IndentContextManager(self)
        self._line_finished = True

    def indent(self):
        self._indent += 1

    def dedent(self):
        self._indent -= 1

    def indented(self) -> IndentContextManager:
        return self._indent_context

    def write_raw(self, content: str):
        self._buffer.write(content)

    def write(self, content: str):
        if self._line_finished:
            self._buffer.write(self._tab * self._indent)
        self._buffer.write(content)
        self._line_finished = content.endswith('\n')

    def writeline(self, line: str):
        self.write(line + '\n')


    def getvalue(self) -> str:
        return self._buffer.getvalue()

    def close(self):
        self._buffer.close()




class _FieldDescriptorProto:
    class Type (Enum):
        # 0 is reserved for errors.
        # Order is weird for historical reasons.
        TYPE_DOUBLE         = 1
        TYPE_FLOAT          = 2
        # Not ZigZag encoded.  Negative numbers take 10 bytes.  Use TYPE_SINT64 if
        # negative values are likely.
        TYPE_INT64          = 3
        TYPE_UINT64         = 4
        # Not ZigZag encoded.  Negative numbers take 10 bytes.  Use TYPE_SINT32 if
        # negative values are likely.
        TYPE_INT32          = 5
        TYPE_FIXED64        = 6
        TYPE_FIXED32        = 7
        TYPE_BOOL           = 8
        TYPE_STRING         = 9
        # Tag-delimited aggregate.
        # Group type is deprecated and not supported in proto3. However, Proto3
        # implementations should still be able to parse the group wire format and
        # treat group fields as unknown fields.
        TYPE_GROUP          = 10
        TYPE_MESSAGE        = 11  # Length-delimited aggregate.

        # New in version 2.
        TYPE_BYTES          = 12
        TYPE_UINT32         = 13
        TYPE_ENUM           = 14
        TYPE_SFIXED32       = 15
        TYPE_SFIXED64       = 16
        TYPE_SINT32         = 17  # Uses ZigZag encoding.
        TYPE_SINT64         = 18  # Uses ZigZag encoding.

    class Label(Enum):
        # 0 is reserved for errors
        LABEL_OPTIONAL = 1
        LABEL_REQUIRED = 2
        LABEL_REPEATED = 3
    
    
_field_type_mapping = {
    _FieldDescriptorProto.Type.TYPE_DOUBLE.value: 'float',
    _FieldDescriptorProto.Type.TYPE_FLOAT.value: 'float',
    _FieldDescriptorProto.Type.TYPE_INT64.value: 'int',
    _FieldDescriptorProto.Type.TYPE_UINT64.value: 'int',
    _FieldDescriptorProto.Type.TYPE_INT32.value: 'int',
    _FieldDescriptorProto.Type.TYPE_FIXED64.value: 'int',
    _FieldDescriptorProto.Type.TYPE_FIXED32.value: 'int',
    _FieldDescriptorProto.Type.TYPE_BOOL.value: 'bool',
    _FieldDescriptorProto.Type.TYPE_STRING.value: 'str',
    _FieldDescriptorProto.Type.TYPE_BYTES.value: 'bytes',
    _FieldDescriptorProto.Type.TYPE_UINT32.value: 'int',
    _FieldDescriptorProto.Type.TYPE_SFIXED32.value: 'int',
    _FieldDescriptorProto.Type.TYPE_SFIXED64.value: 'int',
    _FieldDescriptorProto.Type.TYPE_SINT32.value: 'int',
    _FieldDescriptorProto.Type.TYPE_SINT64.value: 'int',
}


def _get_import_name(type_name: str) -> Tuple[str, str, str]:
    module_name, _, class_name = type_name.rpartition('.')

    # Package name starting with a . is absolute (fully qualified)
    if module_name.startswith('.'):
        module_name = module_name[1:]
    else:
        module_name = '.'+module_name

    import_name = '_' + module_name.replace('_', '__').replace('.', '_')

    module_name += '_pb2'

    return import_name, module_name, class_name


def _field_type(type: int, type_name: str, package_classes: Dict[str, str]) -> str:
    if type in _field_type_mapping:
        return _field_type_mapping[type]

    if type == _FieldDescriptorProto.Type.TYPE_MESSAGE.value:
        if type_name in package_classes:
            return package_classes[type_name]

        import_name, module_name, class_name = _get_import_name(type_name)
        return import_name+'.'+class_name

    if type == _FieldDescriptorProto.Type.TYPE_ENUM.value:
        return 'int'


def _get_map_types(type: DescriptorProto) -> Tuple[Optional[FieldDescriptorProto], Optional[FieldDescriptorProto]]:

    if type is None:
        return None, None

    field_names = set([f.name for f in type.field])  # type: Set[str]
    field_types = {f.name: f for f in type.field }  # type: Dict[str, FieldDescriptorProto]

    if field_names != {'key', 'value'}:
        return None, None

    return field_types['key'], field_types['value']


def _get_field_map_types(type: int, type_name: str, nested_types: Dict[str, DescriptorProto]) -> Tuple[Optional[FieldDescriptorProto], Optional[FieldDescriptorProto]]:
    if type != _FieldDescriptorProto.Type.TYPE_MESSAGE.value or not type_name.endswith('Entry'):
        return None, None

    entry_type = nested_types.get(type_name)
    return _get_map_types(entry_type)


def generate_field(writer: IndentedWriter, descriptor: FieldDescriptorProto, package_classes: Dict[str, str], nested_types: Dict[str, DescriptorProto], syntax: str):

    writer.write('self.%s = None' % descriptor.name)

    if descriptor.label == _FieldDescriptorProto.Label.LABEL_REPEATED.value:
        key, value = _get_field_map_types(descriptor.type, descriptor.type_name, nested_types)
        if key is not None:
            writer.writeline('  # type: _t.Dict[%s, %s]' % (
                _field_type(key.type, key.type_name, package_classes), _field_type(value.type, value.type_name, package_classes)))
        else:
            writer.writeline('  # type: _t.List[%s]' % _field_type(descriptor.type, descriptor.type_name, package_classes))
    elif descriptor.label == _FieldDescriptorProto.Label.LABEL_OPTIONAL.value and syntax == 'proto2':
        writer.writeline('  # type: _t.Optional[%s]' % _field_type(descriptor.type, descriptor.type_name, package_classes))
    else: writer.writeline('  # type: %s' % _field_type(descriptor.type, descriptor.type_name, package_classes))


def generate_class(writer: IndentedWriter, descriptor: DescriptorProto, full_parent_name: str, relative_parent_name: str, package_classes: Dict[str, str], package_descriptor_lookup: Dict[str, DescriptorProto], syntax: str):
    writer.writeline('# noinspection PyAbstractClass')
    writer.writeline('class %s (_message.Message):' % descriptor.name)

    if full_parent_name:
        full_name = full_parent_name + '.' + descriptor.name
    else:
        full_name = descriptor.name

    if relative_parent_name:
        relative_name = relative_parent_name + '.' + descriptor.name
    else:
        relative_name = descriptor.name

    package_classes['.' + full_name] = relative_name
    package_descriptor_lookup['.' + full_name] = descriptor

    nested_types = {}

    with writer.indented():
        writen_any = False

        for enum_type in descriptor.enum_type:
            writen_any = True
            generate_enum(writer, enum_type)

        for nested_type in descriptor.nested_type:
            nested_types['.' + full_name + '.' + nested_type.name] = nested_type

            key, value = _get_map_types(nested_type)
            if key is None:
                writen_any = True
                generate_class(writer, nested_type, full_name, relative_name, package_classes, package_descriptor_lookup, syntax)

        if writen_any:
            writer.writeline('')
            writer.writeline('')

        writer.writeline('DESCRIPTOR = None  # type: _descriptor.Descriptor')

        writer.writeline('')
        writer.writeline('def __init__(self):')
        with writer.indented():
            writen_any = False
            for field in descriptor.field:
                writen_any = True
                generate_field(writer, field, package_classes, nested_types, syntax)

            if not writen_any:
                writer.writeline('pass')

        writer.writeline('')


def generate_enum(writer: IndentedWriter, descriptor):
    pass


def generate_imports(writer: IndentedWriter, proto_file):

    needs_enum = len(proto_file.enum_type) > 0

    def gather_imports(imports: Set[Tuple[str, str]], descriptor):
        nonlocal needs_enum

        needs_enum = needs_enum or len(descriptor.enum_type) > 0

        for field in descriptor.field:
            if field.type == _FieldDescriptorProto.Type.TYPE_MESSAGE.value:
                import_name, module_name, class_name = _get_import_name(field.type_name)
                imports.add((import_name, module_name))

        for nested_type in descriptor.nested_type:
            gather_imports(imports, nested_type)

    imports = set()  # type: Set[Tuple[str, str]]

    for messageD in proto_file.message_type:
        gather_imports(imports, messageD)


    writer.writeline('import typing as _t')
    if needs_enum:
        writer.writeline('import enum')

    writer.writeline('')
    writer.writeline('from google.protobuf import message as _message')
    writer.writeline('from google.protobuf import descriptor as _descriptor')

    writer.writeline('')

    for import_name, module_name in imports:
        writer.writeline('import %s as %s' % (module_name, import_name))



def generate_package(proto_file: FileDescriptorProto, output_file):
    filename, ext = os.path.splitext(proto_file.name)

    output_file.name = '%s_pb2.pyi' % filename

    writer = IndentedWriter()

    writer.writeline('# Generated by the protocol buffer compiler.  DO NOT EDIT!')
    writer.writeline('# source: %s' % proto_file.name)

    generate_imports(writer, proto_file)

    writer.writeline('')
    writer.writeline('DESCRIPTOR = None  # type: _descriptor.FileDescriptor')

    writer.writeline('')
    writer.writeline('')

    for enumD in proto_file.enum_type:
        generate_enum(writer, enumD)
        writer.writeline('')

    package_classes = {}
    package_descriptor_lookup = {}

    for messageD in proto_file.message_type:
        generate_class(writer, messageD, proto_file.package, '', package_classes, package_descriptor_lookup, proto_file.syntax)
        writer.writeline('')



    output_file.content = writer.getvalue()
    writer.close()


def generate_code(request, response):
    for proto_file in request.proto_file:
        output = []

        generate_package(proto_file, response.file.add())


if __name__ == '__main__':
    # Read request message from stdin
    data = sys.stdin.buffer.read()

    # Parse request
    request = plugin.CodeGeneratorRequest()
    request.ParseFromString(data)

    # Create response
    response = plugin.CodeGeneratorResponse()

    # Generate code
    generate_code(request, response)

    # Serialise response message
    output = response.SerializeToString()

    # Write to stdout
    sys.stdout.buffer.write(output)