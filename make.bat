@echo off

cd %~dp0

echo ##################################################################################
echo #  Generating executable
echo #
pyinstaller --onefile .\python-stub.py
if %errorlevel% neq 0 exit /b %errorlevel%

echo ##################################################################################
echo #  Coping executable
echo #
xcopy /y .\dist\python-stub.exe .
if %errorlevel% neq 0 exit /b %errorlevel%

echo ##################################################################################
echo #  Compiling test messages
echo #
protoc @.\protoc_testmessages_options.txt
if %errorlevel% neq 0 exit /b %errorlevel%

echo ##################################################################################
echo #  done
echo #